function createBirthdayList() {
  // table Element erstellen und body von html-Seite hinzufügen
  const elBody = document.querySelector('body');
  const elScript = document.querySelector('body script');
  const elTable = document.createElement('table');
  elBody.insertBefore(elTable, elScript);
  // Header von Tabelle erstellen
  const elTHead = document.createElement('thead');
  const elRowHead = createRowAndAddValues('th', [
    'Vorname',
    'Nachname',
    'Geb.Datum',
  ]);
  elTHead.appendChild(elRowHead);
  // Header table Element hinzufügen
  elTable.appendChild(elTHead);
  // Body von Tabelle erstellen
  const elTBody = document.createElement('tbody');
  // Daten von Schulkamerad*innen
  const data = [
    ['Anna', 'Musterfrau', '13.4.2005'],
    ['Moritz', 'Mustermann', '2.8.2006'],
    ['Frieda', 'Musterfrau', '21.2.2006'],
    ['Max', 'Mustermann', '3.3.2007'],
    ['Marta', 'Musterfrau', '1.12.2006'],
  ];
  // Zeile für alle Kamerad*innen erstellen und
  // Body Element hinzufügen
  for (let i = 0; i < data.length; i++) {
    let elRowBody = createRowAndAddValues('td', data[i]);
    elTBody.appendChild(elRowBody);
  }
  // Body table Element hinzufügen
  elTable.appendChild(elTBody);
}

function createRowAndAddValues(cellTag, values) {
  const elRow = document.createElement('tr');
  for (let i = 0; i < values.length; i++) {
    const elCell = document.createElement(cellTag);
    elCell.textContent = values[i];
    elRow.appendChild(elCell);
  }
  return elRow;
}

createBirthdayList();
